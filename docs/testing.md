# Testing

End-to-End (E2E) testing is done using [Cypress](https://www.cypress.io).

Cypress is open source code and can be used freely.

Tests go into the `cypress/integration/` directory.

[Cypress Documentation](https://docs.cypress.io)

## Running Tests

To run tests, execute the following command from the command line:

    npm run e2e


## Visual Regression Testing

This repository uses [BackstopJS](https://garris.github.io/BackstopJS/) for visual regression testing! BackstopJS works by collecting *reference* images of Template and Page patterns.  Reference images are later compared to the current state after you make changes.

### Prerequisites:

* [Docker and Docker Compose](https://www.docker.com/community-edition#/download) must be installed.

### Workflow:
First, be sure your new work has been committed to your feature branch.
Then, do a build of the dev branch to create reference images

```bash
git checkout dev
npm ci
npm start
npm run backstop:ref
```

To check how your current work compares with the reference screenshots created from the dev branch, check out your feature branch and test it:

```bash
git checkout MYBRANCH
npm ci
npm start
npm run backstop:test
```

At this time, do not commit reference images to the repository. BackstopJS is simply a tool to test for visual regressions during local development.

### Trouble shooting

If you run into an error like:

```
Starting one_web_1 ... error

ERROR: for one_web_1  Cannot start service web: network e452366b7ec262e0c5724bcdf4b398715174b28055d152d327844b81d88c0245 not found

ERROR: for web  Cannot start service web: network e452366b7ec262e0c5724bcdf4b398715174b28055d152d327844b81d88c0245 not found
ERROR: Encountered errors while bringing up the project.
```

Then you will likely need to delete the image container causing the problem. In
this case we will remove the `nginx:stable-alpine` image with:

```
$ docker ps -a | awk '{ print $1,$2 }' | grep nginx:stable-alpine | awk '{print $1 }' | xargs -I {} docker rm {}
```
