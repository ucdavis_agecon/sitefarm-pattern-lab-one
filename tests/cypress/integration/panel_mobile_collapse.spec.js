describe('Panel Mobile Collapse', () => {
  const panel = '.panel--mobile-collapse'

  beforeEach(() => {
    cy.visit('/patterns/01-molecules-02-blocks-00-panel/01-molecules-02-blocks-00-panel.rendered.html')
  })

  context('Desktop', () => {
    it('Panel should display everything', () => {
      cy.get(panel).find('.panel__content').should('be.visible')
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('Panel should hide content and toogle when on clicking the title', () => {
      cy.get(panel).find('.panel__content').should('not.be.visible')
      cy.get(panel).find('.panel__title').click()
      cy.get(panel).find('.panel__content').should('be.visible')
      cy.get(panel).find('.panel__title').click()
      cy.get(panel).find('.panel__content').should('not.be.visible')
    })
  })

  it('Switching between mobile and desktop should not mix styles', () => {
    cy.get(panel).find('.panel__content').should('be.visible')
    cy.viewport(320, 600)
    cy.get(panel).find('.panel__content').should('not.be.visible')
    cy.viewport(1000, 600)
    cy.get(panel).find('.panel__content').should('be.visible')
    cy.viewport(320, 600)
    cy.get(panel).find('.panel__content').should('not.be.visible')
  })

})
