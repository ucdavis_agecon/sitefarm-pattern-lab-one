describe('Quick Links', () => {
  const menu = '.quick-links'

  beforeEach(() => {
    cy.visit('/patterns/01-molecules-05-navigation-02-quick-links/01-molecules-05-navigation-02-quick-links.rendered.html')
  })

  context('Responsive Change', () => {
    it('Menu changes based on screen size', () => {
      cy.viewport(1000, 660)
      cy.get(menu).should('have.css', 'position').and('match', /absolute/)
      cy.viewport(320, 600)
      cy.get(menu).should('have.css', 'position').and('match', /static/)
      cy.viewport(1000, 660)
      cy.get(menu).should('have.css', 'position').and('match', /absolute/)
    })

    it('Open mobile menu closes when moving to desktop', () => {
      cy.viewport(320, 600)
      cy.get('.quick-links__title').click()
      cy.get(menu).should('be.visible')
      cy.viewport(1000, 660)
      cy.get(menu).should('be.hidden')
    })
  })

  context('Desktop', () => {
    it('Quicklinks toggle when clicking the title', () => {
      cy.get(menu).should('be.hidden')
      cy.get('.quick-links__title').click()
      cy.get(menu).should('be.visible')
      cy.get('.quick-links__title').click()
      cy.get(menu).should('be.hidden')
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('Menu toggles open on mobile title click', () => {
      cy.get(menu).should('be.hidden')
      cy.get('.quick-links__title').click()
      cy.get(menu).should('be.visible')
      cy.get('.quick-links__title').click()
      cy.get(menu).should('be.hidden')
    })
  })

})
