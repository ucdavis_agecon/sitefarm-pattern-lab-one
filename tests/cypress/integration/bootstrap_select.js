describe('Bootstrap Select multi select dropdown', () => {
  beforeEach(() => {
    cy.visit('/patterns/00-atoms-05-forms-01-select-menu/00-atoms-05-forms-01-select-menu.rendered.html')
  })

  it('Select dropdown with "multiple" attribute uses the Bootstrap Select library', () => {
    cy.get('.bootstrap-select').should('be.visible')
    cy.get('.dropdown-menu').should('not.be.visible')
    cy.get('.dropdown-toggle').click()
    cy.get('.dropdown-menu').should('be.visible')
    cy.get('.dropdown-toggle').click()
    cy.get('.dropdown-menu').should('not.be.visible')
  })

  it('Bootstrap Select allows multiple selections', () => {
    cy.get('.dropdown-toggle').click()
    cy.get('.dropdown-menu').contains('Option One').click()
    cy.get('.dropdown-menu').contains('Option Two').click()
    cy.get('.filter-option-inner-inner').should('have.text', 'Option One, Option Two')

    cy.get('.dropdown-menu').contains('Option Two').click()
    cy.get('.filter-option-inner-inner').should('have.text', 'Option One')

    cy.get('.dropdown-menu').contains('Option Five').click()
    cy.get('.filter-option-inner-inner').should('have.text', 'Option One, Option Five')
  })
})
