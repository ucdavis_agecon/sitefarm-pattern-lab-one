describe('Accessibility', () => {
  it('Home page', () => {
    cy.visit('/patterns/04-pages-00-homepage/04-pages-00-homepage.rendered.html')

    // Inject the axe-core library.
    cy.injectAxe();

    // Test the Accessibility.
    cy.checkA11y();
  })

  it('Basic page', () => {
    cy.visit('/patterns/04-pages-01-basic-page/04-pages-01-basic-page.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Basic page Featured', () => {
    cy.visit('/patterns/04-pages-01-basic-page-featured/04-pages-01-basic-page-featured.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Article', () => {
    cy.visit('/patterns/04-pages-02-article/04-pages-02-article.rendered.html')
    cy.injectAxe();
    cy.checkA11y({
      // This can be removed once https://github.com/dequelabs/axe-core/pull/2092 is resolved.
      rules: {
        'landmark-no-duplicate-contentinfo': { 'enabled': false }
      }
    });
  })

  it('Article Landing', () => {
    cy.visit('/patterns/04-pages-03-articles-landing/04-pages-03-articles-landing.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Event', () => {
    cy.visit('/patterns/04-pages-04-event/04-pages-04-event.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Event Landing', () => {
    cy.visit('/patterns/04-pages-05-event-landing/04-pages-05-event-landing.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Taxonomy Term', () => {
    cy.visit('/patterns/04-pages-06-taxonomy-term/04-pages-06-taxonomy-term.rendered.html')
    cy.injectAxe();
    // Ignore Heading Order since this can't be changed.
    cy.checkA11y({
      rules: {
        'heading-order': { 'enabled': false }
      }
    });
  })

  it('Person', () => {
    cy.visit('/patterns/04-pages-07-person/04-pages-07-person.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Person Directory', () => {
    cy.visit('/patterns/04-pages-08-person-directory/04-pages-08-person-directory.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Search', () => {
    cy.visit('/patterns/04-pages-09-search/04-pages-09-search.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Photo Gallery', () => {
    cy.visit('/patterns/04-pages-10-photo-gallery/04-pages-10-photo-gallery.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Photo Gallery Page', () => {
    cy.visit('/patterns/04-pages-11-photo-gallery-page/04-pages-11-photo-gallery-page.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('CAS Login', () => {
    cy.visit('/patterns/04-pages-12-cas-login/04-pages-12-cas-login.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })

  it('Branding Elements', () => {
    cy.visit('/patterns/03-templates-14-branding-elements/03-templates-14-branding-elements.rendered.html')
    cy.injectAxe();
    cy.checkA11y();
  })
})
