describe('Primary Navigation', () => {
  beforeEach(() => {
    cy.visit('/patterns/01-molecules-05-navigation-00-primary-nav-dropdowns/01-molecules-05-navigation-00-primary-nav-dropdowns.rendered.html')
  })

  it('Menu changes based on screen size', () => {
    const listItem = '.menu > li'

    cy.viewport(1000, 660)
    cy.get('.menu').should('have.class', 'sf-js-enabled')
    cy.get(listItem).should('have.css', 'float').and('match', /left/)
    cy.viewport(320, 600)
    cy.get('.menu').should('not.have.class', 'sf-js-enabled')
    cy.get(listItem).should('have.css', 'float').and('match', /none/)
    cy.viewport(1000, 660)
    cy.get('.menu').should('have.class', 'sf-js-enabled')
    cy.get(listItem).should('have.css', 'float').and('match', /left/)
    cy.viewport(320, 600)
    cy.get('.menu').should('not.have.class', 'sf-js-enabled')
    cy.get(listItem).should('have.css', 'float').and('match', /none/)
  })

  context('Desktop', () => {
    it('Superfish is enabled', () => {
      cy.get('.menu').should('have.class', 'sf-js-enabled')
    })

    it('Hovering over a menu item with submenus should open the submenu', () => {
      const subMenu = '.menu > :nth-child(1) > .menu'

      cy.get(subMenu).should('not.be.visible')
      // Simulate hovering on the first element by using a Focus.
      cy.get('.menu > :nth-child(1) > .sf-with-ul').focus()
      cy.get(subMenu).should('be.visible')

      // Check that the sub-sub menu works
      cy.get(subMenu).find('li .menu').should('not.be.visible')
      cy.get(subMenu).find('li .sf-with-ul').focus()
      cy.get(subMenu).find('li .menu').should('be.visible')

      // Moving focus off the submenu closes it
      cy.get('.sf-js-enabled > :nth-child(2) > a').focus()
      cy.get(subMenu).find('li .menu').should('not.be.visible')
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('Superfish is disabled', () => {
      cy.get('.menu').should('not.have.class', 'sf-js-enabled')
    })

    it('Toggle submenu', () => {
      const subMenu = '.menu > :nth-child(1) > .menu'
      const toggle = ':nth-child(1) > :nth-child(1) > .submenu-toggle'

      cy.get(subMenu).should('not.be.visible')
      cy.get(toggle).click()
      cy.get(subMenu).should('be.visible')
      cy.get(toggle).click()
      cy.get(subMenu).should('not.be.visible')
    })

    it('Toggle sub-sub menu', () => {
      const subMenu = '.menu > :nth-child(1) > .menu'
      const toggle = ':nth-child(1) > :nth-child(1) > .submenu-toggle'
      const subToggle = ':nth-child(3) > :nth-child(1) > .submenu-toggle'

      cy.get(toggle).click()
      cy.get(subMenu).find('li .menu').should('not.be.visible')
      cy.get(subToggle).click()
      cy.get(subMenu).find('li .menu').should('be.visible')
      cy.get(subToggle).click()
      cy.get(subMenu).find('li .menu').should('not.be.visible')
    })
  })

})
