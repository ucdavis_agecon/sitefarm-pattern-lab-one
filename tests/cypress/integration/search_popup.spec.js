describe('Search Popup', () => {
  const form = '.search-popup'

  beforeEach(() => {
    cy.visit('/patterns/01-molecules-06-components-12-search-popup/01-molecules-06-components-12-search-popup.rendered.html')
  })

  context('Responsive Change', () => {
    it('Form changes based on screen size', () => {
      cy.get(form).should('be.hidden')
      cy.viewport(320, 600)
      cy.get(form).should('be.visible')
      cy.viewport(1000, 660)
      cy.get(form).should('be.hidden')
    })
  })

  context('Desktop', () => {
    it('Search Popup toggle should be visible', () => {
      cy.get('.search-popup__open').should('be.visible')
    })

    it('Open and close the form', () => {
      cy.get(form).should('be.hidden')
      cy.get('.search-popup__open').click()
      cy.get(form).should('be.visible')
      cy.get('.search-popup__close').click()
      cy.get(form).should('be.hidden')

    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('Search Popup toggle is hidden in mobile', () => {
      cy.get('.search-popup__open').should('be.hidden')
    })
  })

})
