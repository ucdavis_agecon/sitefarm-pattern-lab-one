// Note: add a scroll duration so that the scroll event will trigger.
describe('Sticky Scroll Header', () => {
  beforeEach(() => {
    cy.visit('/patterns/04-pages-01-basic-page/04-pages-01-basic-page.rendered.html')
  })

  it('Header should be fixed to the top on scroll', () => {
    cy.get('.l-header--fixed').should('not.have.class', 'is-fixed')
    cy.scrollTo('bottom', { duration: 50 })
    cy.get('.l-header--fixed').should('have.class', 'is-fixed')
    cy.scrollTo('top', { duration: 50 })
    cy.get('.l-header--fixed').should('not.have.class', 'is-fixed')
  })


  it('Menu should be fixed to the top on scroll', () => {
    cy.get('.navbar').should('not.have.class', 'is-fixed')
    cy.scrollTo('bottom', { duration: 50 })
    cy.get('.navbar').should('have.class', 'is-fixed')
    cy.scrollTo('top', { duration: 50 })
    cy.get('.navbar').should('not.have.class', 'is-fixed')
  })

  it('MegaMenu should remove extra padding on the .l-main selector when scrolling', () => {
    // First check that there is a top padding when scrolling.
    cy.scrollTo('bottom', { duration: 50 })
    cy.get('.l-main').should('not.have.css', 'padding-top', '0px')
    cy.scrollTo('top', { duration: 50 })

    // Once adding the .has-mega class to the body, the padding should go away.
    cy.scrollTo('bottom', { duration: 50 }).then(() => {
      Cypress.$('body').addClass('has-mega')
      cy.get('.l-main').should('have.css', 'padding-top', '0px')
    })
  })

})
