describe('Mobile Nav Toggle', () => {
  const toggle = '.js-nav-toggle'

  beforeEach(() => {
    cy.visit('/patterns/02-organisms-00-global-00-header/02-organisms-00-global-00-header.rendered.html')
  })

  context('Desktop', () => {
    it('Mobile Nav toggle is hidden in Desktop', () => {
      cy.get(toggle).should('not.be.visible')
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('Mobile Nav toggle should be visible', () => {
      cy.get(toggle).should('be.visible')
    })

    it('Off-canvas menu should be hidden by default', () => {
      cy.get('.off-canvas').should('not.be.visible')
    })

    it('Toggle gets an active class when clicked', () => {
      cy.get(toggle).click()
        .should('have.class', 'nav-toggle--active')
        .click()
        .should('not.have.class', 'nav-toggle--active')
        .click()
        .should('have.class', 'nav-toggle--active')
    })

    it('Menu opens and closes when toggling', () => {
      cy.get(toggle).click()
      cy.get('.navbar').should('have.class', 'menu--open')
      cy.get('.off-canvas').should('be.visible')
      cy.get(toggle).click()
      cy.get('.navbar').should('have.class', 'menu--closed')
      cy.get(toggle).click()
      cy.get('.navbar').should('have.class', 'menu--open')
    })

  })
})
