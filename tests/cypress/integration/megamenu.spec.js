describe('MegaMenu', () => {
  beforeEach(() => {
    cy.visit('/patterns/01-molecules-05-navigation-00-primary-nav-megamenu/01-molecules-05-navigation-00-primary-nav-megamenu.rendered.html')
  })

  context('Responsive Change', () => {
    it('MegaMenu is being used', () => {
      cy.get('body').should('have.class', 'has-mega')
      cy.get('.primary-nav')
        .should('have.class', 'primary-nav--mega')
        .should('have.class', 'primary-nav--justify')
      cy.viewport(320, 600)
      cy.get('body').should('have.class', 'has-mega')
      cy.get('.primary-nav')
        .should('have.class', 'primary-nav--mega')
        .should('have.class', 'primary-nav--justify')
    })

    it('Menu changes based on screen size', () => {
      cy.viewport(1000, 660)
      cy.get('.primary-nav').should('have.css', 'overflow', 'hidden')
      cy.viewport(320, 600)
      cy.get('.primary-nav').should('have.css', 'overflow', 'visible')
      cy.viewport(1000, 660)
      cy.get('.primary-nav').should('have.css', 'overflow', 'hidden')
      cy.viewport(320, 600)
      cy.get('.primary-nav').should('have.css', 'overflow', 'visible')
    })

    it('The desktop submenus should not be hidden once hidden in mobile', () => {
      const subMenu = '.menu > :nth-child(1) > .menu'
      const toggle = ':nth-child(1) > :nth-child(1) > .submenu-toggle'

      cy.viewport(320, 600)
      cy.get(toggle).click()
      cy.get(subMenu).should('be.visible')
      cy.get(toggle).click()
      cy.get(subMenu).should('be.hidden')
      // Wait for the submenu toggle to finish animating.
      cy.wait(300)
      cy.viewport(1000, 660)
      cy.get('.primary-nav').should('have.css', 'overflow', 'hidden')
      cy.get(subMenu).should('be.visible')
    })
  })

  context('Desktop', () => {
    it('Hovering over a menu item with submenus should open the submenu', () => {
      const firstLink = '#primary-nav > :nth-child(1) > :nth-child(1) > a'
      const subMenu = '.menu > :nth-child(1) > .menu'

      // Don't check visibility since a height of 0 is still technically visible.
      cy.get(subMenu).invoke('height').should('be', 0)
      // Simulate hovering on the first element by using a Focus.
      cy.get(firstLink).focus()
      cy.get(subMenu).should('be.visible')
      cy.get(subMenu).invoke('height').should('be.gt', 0)
      cy.get('.primary-nav').should('have.class', 'is-hover')

      // Sub-sub menu should be hidden
      cy.get(subMenu).find('li .menu').should('be.hidden')

      // Moving focus off the submenu closes it
      cy.get(firstLink).blur()
      cy.get('.primary-nav').should('not.have.class', 'is-hover')
      cy.get(subMenu).invoke('height').should('be', 0)
    })
  })

  context('Mobile', () => {
    beforeEach(function() {
      cy.viewport(320, 600)
    });

    it('MegaMenu is disabled', () => {
      cy.get('.primary-nav').should('have.css', 'overflow', 'visible')
    })

    it('Toggle submenu', () => {
      const subMenu = '.menu > :nth-child(1) > .menu'
      const toggle = ':nth-child(1) > :nth-child(1) > .submenu-toggle'

      cy.get(subMenu).should('not.be.visible')
      cy.get(toggle).click()
      cy.get(subMenu).should('be.visible')
      cy.get(toggle).click()
      cy.get(subMenu).should('not.be.visible')
    })

    it('Toggle sub-sub menu', () => {
      const subMenu = '.menu > :nth-child(1) > .menu'
      const toggle = ':nth-child(1) > :nth-child(1) > .submenu-toggle'
      const subToggle = ':nth-child(3) > :nth-child(1) > .submenu-toggle'

      cy.get(toggle).click()
      cy.get(subMenu).find('li .menu').should('not.be.visible')
      cy.get(subToggle).click()
      cy.get(subMenu).find('li .menu').should('be.visible')
      cy.get(subToggle).click()
      cy.get(subMenu).find('li .menu').should('not.be.visible')
    })
  })

})
