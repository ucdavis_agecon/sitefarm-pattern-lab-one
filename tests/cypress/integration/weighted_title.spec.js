describe('Weighted Title', () => {
  beforeEach(() => {
    cy.visit('/patterns/01-molecules-02-blocks-00-panel/01-molecules-02-blocks-00-panel.rendered.html')
  })

  it('The first word should be wrapped in a weighted class', () => {
    cy.get('.panel__title--weighted').should('have.text', 'Panel')
  })
})
