{
  "title": "Joe Smith",
  "bodyClass": "person",
  "emailAdd": "hgnafario@ucdavis.edu",
  "webLink": "neurosci.ucdavis.edu",
  "excerpt": {
    "long": "A life long student of the Neurological sciences, I Dr. Herbert Nafario have unlocked the secrets of the human mind. After 20 years of hard research and many many grants I have accomplished what other scientists could only dream of. I also enjoy the outdoors and pizza. "
  },
  "listDownloads": [
    {
      "listDownload": "Mind Map White Paper"
    },
    {
      "listDownload": "2016 Neuro Report"
    },
    {
      "listDownload": "My Personal Memoirs"
    }
  ],
  "breadcrumbs": [
    {
      "url": "../04-pages-00-homepage/04-pages-00-homepage.html",
      "linkTitle": "Home"
    },
    {
      "url": "",
      "linkTitle": "Directory"
    },
    {
      "url": "",
      "linkTitle": "Joe Smith"
    }
  ],
  "img": {
    "profileImage": {
      "src": "../../images/sample/person/person-04-profile.jpg",
      "alt": "Dr. Nafario"
    }
  },
  "person": {
    "title": "Faculty",
    "dept": "College of Neurological Sciences",
    "location": "Room 22 Legendary Hall",
    "address": true,
    "officeHours": "9AM-5PM"
  },
  "pageCopy": {
    "section01": {
      "heading": "",
      "text": "<p>Just a few hours after its birth, the long-legged brown foal stands in its stall, appearing on first glance to be sound, sturdy and healthy. But something is very wrong with this newborn horse.</p><p>The foal seems detached, stumbles towards people and doesn’t seem to recognize its mother or have any interest in nursing. It even tries to climb into the corner feeder.</p><p>The bizarre symptoms are characteristic of a syndrome that has puzzled horse owners and veterinarians for a century. But recently, UC Davis researchers have discovered a surprising clue to the syndrome and intriguing similarities to childhood autism in humans.</p>"
    },
    "section02": {
      "heading": "Resembles children with autism",
      "text": "<p>“The behavioral abnormalities in these foals seem to resemble some of the symptoms in children with autism,” said John Madigan, a UC Davis veterinary professor and an expert in equine neonatal health.</p> <p>“There are thousands of potential causes for autism, but the one thing that all autistic children have in common is that they are detached,” said Isaac Pessah, a professor of molecular biosciences at the UC Davis School of Veterinary Medicine and a faculty member of the UC Davis MIND Institute, who investigates environmental factors that may play a role in the development of autism in children.</p> <p>Pessah, Madigan and other researchers in veterinary and human medicine recently formed a joint research group and secured funding to investigate whether abnormal levels of neurosteroids — a group of chemicals that modulate perception — may play a role in both disorders.</p> <p>They hope their efforts will help prevent and treat the disorder in foals and advance the search for the causes of autism, which affects more than 3 million individuals in the United States.</p>"
    },
    "section03": {
      "heading": "Maladjusted foal syndrome",
      "text": "<p>In newborn foals, the disorder known as neonatal maladjustment syndrome or dummy foal syndrome occurs in only 3 to 5 percent of live births. But when it does appear, it is, said one Thoroughbred horse breeder, “a nightmare.”</p> <p>With continuous treatment, including around-the-clock bottle or tube feeding plus intensive care in a veterinary clinic, 80 percent of the foals recover. But that level of care — required for up to a week or 10 days — is grueling and costly.</p> <p>For years, the syndrome has been attributed to hypoxia — insufficient oxygen during the birthing process. Typically, when a foal’s brain is deprived of oxygen, the resulting effects include mental deficits, abnormal behavior, blindness and even seizures. And, as in human babies, much of the damage is serious and permanent.</p>"
    },
    "section04": {
      "heading": "But is oxygen deprivation the culprit?",
      "text": "<p>Oddly, however, most foals with neonatal maladjustment syndrome survive the ordeal and have no lingering health problems. This raised the question of whether hypoxia was the culprit in the syndrome, and Madigan and UC Davis veterinary neurologist Monica Aleman began sleuthing around for other potential causes.</p> <p>One of their prime suspects was a group of naturally occurring neurosteroids, which are key to sustaining pregnancies in horses, especially in keeping the foal “quiet” before birth.</p>"
    },
    "section05": {
      "heading": "No galloping in the womb",
      "text": "<p>“Foals don’t gallop in utero,” Madigan is fond of saying, pointing out the dangers to the mare if a four-legged, hoofed fetus were to suddenly become active in the womb. The prenatal calm is made possible, he explained, by neurosteroids that act as sedatives for the unborn foal.</p> <p>However, immediately after birth, the infant horse must make an equally important transition to consciousness. In nature, a baby horse would be easy prey for many natural enemies, so the foal must be ready to run just a few hours after it is born.</p>"
    },
    "section06": {
      "heading": "Biochemical ‘on switch’",
      "text": "<p>In short, somewhere between the time a foal enters the birth canal and the moment it emerges from the womb, a biochemical “on switch” must be flicked that enables the foal to recognize the mare, nurse, and become mobile. Madigan and Aleman suspect that the physical pressure of the birthing process may be that important signal.</p> <p>“We believe that the pressure of the birth canal during the second stage of labor, which is supposed to last 20 to 40 minutes, is an important signal that tells the foal to quit producing the sedative neurosteroids and ‘wake up,’ ” Madigan said.</p>"
    }
  },
  "articleList": [
    {
      "img": {
        "thumbnail": {
          "src": "../../images/sample/article/almonds.jpg",
          "alt": "almonds"
        }
      },
      "headline": {
        "medium": "Almonds contribute little to carbon emissions, study finds"
      },
      "year": {
        "long": "2015"
      },
      "month": {
        "long": "July"
      },
      "day": {
        "short": "24"
      }
    },
    {
      "img": {
        "thumbnail": {
          "src": "../../images/sample/article/warbler.jpg",
          "alt": "warbler"
        }
      },
      "headline": {
        "medium": "Keep Tahoe blue? Less algae, not clarity, key factor for blueness"
      },
      "year": {
        "long": "2015",
        "short": "15"
      },
      "month": {
        "long": "July",
        "short": "Jul",
        "digit": "07"
      },
      "day": {
        "long": "23",
        "short": "23",
        "ordinal": "rd"
      }
    },
    {
      "img": {
        "thumbnail": {
          "src": "../../images/sample/article/vole.jpg",
          "alt": "vole"
        }
      },
      "headline": {
        "medium": "Lyme disease subverts immune system, prevents future protection"
      },
      "year": {
        "long": "2015",
        "short": "15"
      },
      "month": {
        "long": "July",
        "short": "Jul",
        "digit": "07"
      },
      "day": {
        "long": "02",
        "short": "2",
        "ordinal": "nd"
      }
    }
  ]
}
